module.exports = {
    selenium: {
        start_process: true,
        server_path: require('selenium-server').path,
        host: '127.0.0.1',
        port: 4444,
        cli_args: {
            'webdriver.gecko.driver': require('geckodriver').path
        }
    },

    test_settings: {
        firefox: {
            selenium_port: 4444,
            selenium_host: 'localhost',
            silent: true,
            desiredCapabilities: {
                browserName: "firefox",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
    }
}