// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'default e2e test': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.salary-card', 5000)
      .assert.elementPresent('.salary-card')
      .end()
  },

  'modal shows "Success" result test': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.salary-card', 5000)
      .setValue('.tab--employer form .salary-form__input', 100)
      .click('.tab--employer form .salary-form__button')
      .click('.salary-card .nav-item:nth-of-type(2)')
      .setValue('.tab--employee form .salary-form__input', 99)
      .click('.tab--employee form .salary-form__button')
      .waitForElementVisible('.result-modal__title', 5000)
      .assert.containsText('.result-modal__title', 'Success')
      .end()
  },

  'modal shows "Failure" result test': browser => {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible('.salary-card', 5000)
      .setValue('.tab--employer form .salary-form__input', 99)
      .click('.tab--employer form .salary-form__button')
      .click('.salary-card .nav-item:nth-of-type(2)')
      .setValue('.tab--employee form .salary-form__input', 100)
      .click('.tab--employee form .salary-form__button')
      .waitForElementVisible('.result-modal__title', 5000)
      .assert.containsText('.result-modal__title', 'Failure')
      .end()
  }
}