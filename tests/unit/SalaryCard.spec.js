import { shallowMount } from '@vue/test-utils';
import SalaryCard from '@/components/SalaryCard.vue';

describe('SalaryCard.vue', () => {

    const mount = (props) => shallowMount(SalaryCard, {
        propsData: props,
        stubs: {
            'b-card': true,
            'b-tabs': true,
            'b-tab': true,
        },
    });

    it('renders correctly', () => {
        const wrapper = mount({});

        expect(wrapper.element).toMatchSnapshot();
    });
});
