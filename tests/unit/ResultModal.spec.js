import { shallowMount } from '@vue/test-utils';
import ResultModal from '@/components/ResultModal.vue';

describe('ResultModal.vue', () => {

    const mount = (props) => shallowMount(ResultModal, {
        propsData: props,
        stubs: {
            'b-modal': true,
        },
    });

    it('renders correctly', () => {
        const wrapper = mount({
            show: true,
        });

        expect(wrapper.contains('.result-modal__title')).toBeTruthy();
        expect(wrapper.element).toMatchSnapshot();
    });

    it('shows success title', () => {
        const wrapper = mount({
            willingSalary: 100,
            expectedSalary: 99,
        });

        expect(wrapper.find('.result-modal__title').text()).toEqual('Success');
    });

    it('shows failure title', () => {
        const wrapper = mount({
            willingSalary: 99,
            expectedSalary: 100,
        });

        expect(wrapper.find('.result-modal__title').text()).toEqual('Failure');
    });
});
