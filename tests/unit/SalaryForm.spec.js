import { shallowMount } from '@vue/test-utils';
import SalaryForm from '@/components/SalaryForm.vue';

describe('SalaryForm.vue', () => {

    const mount = (props) => shallowMount(SalaryForm, {
        propsData: props,
    });

    it('renders correclty', () => {
        const wrapper = mount({});

        expect(wrapper.contains('.salary-form__button')).toBeTruthy();
        expect(wrapper.element).toMatchSnapshot();
    });

    it('shows error when invalid', () => {
        const wrapper = mount({
            salary: -100,
        });

        wrapper.find('.salary-form').trigger('submit');
        expect(wrapper.find('.salary-form__input').classes()).toContain('is-invalid');
    });

    it('shows message when submited', () => {
        const wrapper = mount({
            show: false,
        });

        expect(wrapper.find('.message').text()).toEqual('Form was submitted');
    });
});
